package core.model;

import javafx.beans.property.*;

import java.util.Date;

/**
 * Created by BigZy on 11.01.2018.
 */
public class Employee {
    private IntegerProperty id;
    private StringProperty fullName;
    private SimpleObjectProperty<Date> dateBirth;
    private StringProperty gender;
    private StringProperty profession;
    private StringProperty categoryRate;
    private IntegerProperty rate;
    private SimpleObjectProperty<Date> dateStartOfWork;
    private StringProperty address;

    public Employee(Integer id, String fullName, Date dateBirth, String gender, String profession, String categoryRate, Integer rate, Date dateStartOfWork, String address) {
        this.id = new SimpleIntegerProperty(id);
        this.fullName = new SimpleStringProperty(fullName);
        this.dateBirth = new SimpleObjectProperty(dateBirth);
        this.gender = new SimpleStringProperty(gender);
        this.profession = new SimpleStringProperty(profession);
        this.categoryRate = new SimpleStringProperty(categoryRate);
        this.rate = new SimpleIntegerProperty(rate);
        this.dateStartOfWork = new SimpleObjectProperty(dateStartOfWork);
        this.address = new SimpleStringProperty(address);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getFullName() {
        return fullName.get();
    }

    public StringProperty fullNameProperty() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName.set(fullName);
    }

    public Date getDateBirth() {
        return dateBirth.get();
    }

    public SimpleObjectProperty<Date> dateBirthProperty() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth.set(dateBirth);
    }

    public String getGender() {
        return gender.get();
    }

    public StringProperty genderProperty() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender.set(gender);
    }

    public String getProfession() {
        return profession.get();
    }

    public StringProperty professionProperty() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession.set(profession);
    }

    public String getCategoryRate() {
        return categoryRate.get();
    }

    public StringProperty categoryRateProperty() {
        return categoryRate;
    }

    public void setCategoryRate(String categoryRate) {
        this.categoryRate.set(categoryRate);
    }

    public int getRate() {
        return rate.get();
    }

    public IntegerProperty rateProperty() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate.set(rate);
    }

    public Date getDateStartOfWork() {
        return dateStartOfWork.get();
    }

    public SimpleObjectProperty<Date> dateStartOfWorkProperty() {
        return dateStartOfWork;
    }

    public void setDateStartOfWork(Date dateStartOfWork) {
        this.dateStartOfWork.set(dateStartOfWork);
    }

    public String getAddress() {
        return address.get();
    }

    public StringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }
}
