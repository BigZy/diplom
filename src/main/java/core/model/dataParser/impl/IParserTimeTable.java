package core.model.dataParser.impl;

import core.model.TimeTableEmployee;
import javafx.collections.ObservableList;

/**
 * Created by ADMIN on 20.04.2018.
 */
public interface IParserTimeTable {
    ObservableList<TimeTableEmployee> getTimeTableEmployees();

    void loadTimeTableEmployees();
    void saveTimeTableEmployees(ObservableList<TimeTableEmployee> TimeTableEmployees);
}
