package core.model.dataParser.impl;

import core.model.Employee;
import javafx.collections.ObservableList;

/**
 * Created by BigZy on 20.01.2018.
 */
public interface IParserEmployees {
    ObservableList<Employee> getEmployees();

    void loadEmployees();
    void saveEmployees(ObservableList<Employee> employees);
}
