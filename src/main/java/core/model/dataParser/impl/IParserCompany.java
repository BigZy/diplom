package core.model.dataParser.impl;

import core.model.Company;

/**
 * Created by BigZy on 11.05.2018.
 */
public interface IParserCompany {
    Company getCompany();

    void loadCompany();
    void saveCompany(Company company);
}
