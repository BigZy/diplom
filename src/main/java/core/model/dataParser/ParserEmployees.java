package core.model.dataParser;

import core.model.dataParser.impl.IParserEmployees;

/**
 * Created by BigZy on 20.01.2018.
 */
public class ParserEmployees {
    IParserEmployees employeesContext;

    public ParserEmployees(IParserEmployees employeesContext) {
        this.employeesContext = employeesContext;
    }

    public IParserEmployees getEmployeesContext() {
        return employeesContext;
    }
}
