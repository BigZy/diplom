package core.model.dataParser;

import core.model.dataParser.impl.IParserTimeTable;

/**
 * Created by ADMIN on 20.04.2018.
 */
public class ParserTimeTable {
    IParserTimeTable timeTableEmployeesContext;

    public ParserTimeTable(IParserTimeTable timeTableEmployeesContext) {
        this.timeTableEmployeesContext = timeTableEmployeesContext;
    }

    public IParserTimeTable getTimeTableEmployeesContext() {
        return timeTableEmployeesContext;
    }
}
