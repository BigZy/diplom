package core.model.dataParser.json;

import com.google.gson.Gson;
import core.model.Company;
import core.model.dataParser.impl.IParserCompany;
import org.apache.log4j.Logger;
import org.hildan.fxgson.FxGson;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * Created by BigZy on 11.05.2018.
 */
public class JsonCompanyParser implements IParserCompany {
    private Company company;

    private static Gson gson;

    private static final Logger _log = Logger.getLogger(JsonCompanyParser.class);

    @Override
    public Company getCompany() {
        return company;
    }

    @Override
    public void loadCompany() {
        gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        try (InputStreamReader reader = new InputStreamReader(new FileInputStream("resources/data/company.json"), StandardCharsets.UTF_8)) {
            company = gson.fromJson(reader, Company.class);
        } catch (FileNotFoundException e) {
            _log.info(e.getMessage());
        } catch (IOException e) {
            _log.info(e.getMessage());
        }
    }

    @Override
    public void saveCompany(Company company) {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("resources/data/company.json"), StandardCharsets.UTF_8)) {
            gson.toJson(company, writer);
        } catch (IOException e) {
            _log.info(e.getMessage());
        }
    }
}
