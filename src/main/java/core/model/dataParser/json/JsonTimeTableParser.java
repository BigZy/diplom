package core.model.dataParser.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.model.Employee;
import core.model.TimeTableEmployee;
import core.model.dataParser.impl.IParserTimeTable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import org.hildan.fxgson.FxGson;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static core.App.getParserEmployees;

/**
 * Created by ADMIN on 21.04.2018.
 */
public class JsonTimeTableParser implements IParserTimeTable {
    private List<TimeTableEmployee> timeTableEmployees = new ArrayList<>();
    private static Gson gson;
    private LocalDate date = LocalDate.now();
    private File file = new File("resources/data/timetable/" +
            date.getYear() + "-" + date.getMonth() + ".json");

    private static final Logger _log = Logger.getLogger(JsonTimeTableParser.class);

    @Override
    public ObservableList<TimeTableEmployee> getTimeTableEmployees() {
        return FXCollections.observableArrayList(timeTableEmployees);
    }

    @Override
    public void loadTimeTableEmployees() {
        gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        String[] days = new String[date.lengthOfMonth()];

        for (int i = 0; i < days.length; i++) {
            days[i] = "0";
        }

        if (!file.exists()) {
            List<Employee> employees = getParserEmployees().getEmployeesContext().getEmployees();
            employees.forEach(e -> {
                timeTableEmployees.add(new TimeTableEmployee(e, days, 0));
            });

            saveTimeTableEmployees(getTimeTableEmployees());
        } else {
            try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8)) {
                Type listType = new TypeToken<List<TimeTableEmployee>>() {}.getType();
                timeTableEmployees = gson.fromJson(reader, listType);
            } catch (FileNotFoundException e) {
                _log.info(e.getMessage());
            } catch (IOException e) {
                _log.info(e.getMessage());
            }
        }
}

    @Override
    public void saveTimeTableEmployees(ObservableList<TimeTableEmployee> timeTableEmployees) {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            gson.toJson(timeTableEmployees, writer);
            this.timeTableEmployees.clear();
            this.timeTableEmployees.addAll(timeTableEmployees);
        } catch (IOException e) {
            _log.info(e.getMessage());
        }
    }
}
