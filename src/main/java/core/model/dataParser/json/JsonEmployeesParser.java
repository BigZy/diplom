package core.model.dataParser.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.model.Employee;
import core.model.dataParser.impl.IParserEmployees;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.Logger;
import org.hildan.fxgson.FxGson;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BigZy on 20.01.2018.
 */
public class JsonEmployeesParser implements IParserEmployees {
    private List<Employee> employees = new ArrayList<>();

    private static Gson gson;

    private static final Logger _log = Logger.getLogger(JsonEmployeesParser.class);

    @Override
    public ObservableList<Employee> getEmployees() {
        return FXCollections.observableArrayList(employees);
    }

    @Override
    public void loadEmployees() {
        gson = FxGson.coreBuilder().setPrettyPrinting().disableHtmlEscaping().create();

        try (InputStreamReader reader = new InputStreamReader(new FileInputStream("resources/data/employees.json"), StandardCharsets.UTF_8)) {
            Type listType = new TypeToken<List<Employee>>() {}.getType();
            employees = gson.fromJson(reader, listType);
        } catch (FileNotFoundException e) {
            _log.info(e.getMessage());
        } catch (IOException e) {
            _log.info(e.getMessage());
        }
    }

    @Override
    public void saveEmployees(ObservableList<Employee> employees) {
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("resources/data/employees.json"), StandardCharsets.UTF_8)) {
            gson.toJson(employees, writer);
            this.employees.clear();
            this.employees.addAll(employees);
        } catch (IOException e) {
            _log.info(e.getMessage());
        }
    }
}
