package core.model.dataParser;

import core.model.dataParser.impl.IParserCompany;

/**
 * Created by BigZy on 11.05.2018.
 */
public class ParserCompany {
    IParserCompany companyContext;

    public ParserCompany(IParserCompany companyContext) {
        this.companyContext = companyContext;
    }

    public IParserCompany getCompanyContext() {
        return companyContext;
    }
}
