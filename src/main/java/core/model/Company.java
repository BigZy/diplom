package core.model;

/**
 * Created by BigZy on 11.05.2018.
 */
public class Company {
    private String name, inn, ogrn, ogrnip, imns, code, pfr, fss, foms, typeTax, kpp, oktmo, okved, okpo, okopf, leader, accountant;

    public Company(String name, String inn, String ogrn, String ogrnip, String imns, String code, String pfr, String fss, String foms, String typeTax, String kpp, String oktmo, String okved, String okpo, String okopf, String leader, String accountant) {
        this.name = name;
        this.inn = inn;
        this.ogrn = ogrn;
        this.ogrnip = ogrnip;
        this.imns = imns;
        this.code = code;
        this.pfr = pfr;
        this.fss = fss;
        this.foms = foms;
        this.typeTax = typeTax;
        this.kpp = kpp;
        this.oktmo = oktmo;
        this.okved = okved;
        this.okpo = okpo;
        this.okopf = okopf;
        this.leader = leader;
        this.accountant = accountant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    public String getOgrnip() {
        return ogrnip;
    }

    public void setOgrnip(String ogrnip) {
        this.ogrnip = ogrnip;
    }

    public String getImns() {
        return imns;
    }

    public void setImns(String imns) {
        this.imns = imns;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPfr() {
        return pfr;
    }

    public void setPfr(String pfr) {
        this.pfr = pfr;
    }

    public String getFss() {
        return fss;
    }

    public void setFss(String fss) {
        this.fss = fss;
    }

    public String getFoms() {
        return foms;
    }

    public void setFoms(String foms) {
        this.foms = foms;
    }

    public String getTypeTax() {
        return typeTax;
    }

    public void setTypeTax(String typeTax) {
        this.typeTax = typeTax;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getOktmo() {
        return oktmo;
    }

    public void setOktmo(String oktmo) {
        this.oktmo = oktmo;
    }

    public String getOkved() {
        return okved;
    }

    public void setOkved(String okved) {
        this.okved = okved;
    }

    public String getOkpo() {
        return okpo;
    }

    public void setOkpo(String okpo) {
        this.okpo = okpo;
    }

    public String getOkopf() {
        return okopf;
    }

    public void setOkopf(String okopf) {
        this.okopf = okopf;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getAccountant() {
        return accountant;
    }

    public void setAccountant(String accountant) {
        this.accountant = accountant;
    }
}
