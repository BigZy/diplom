package core.model.reports;

import core.model.reports.impl.IReportEmployees;

/**
 * Created by BigZy on 04.02.2018.
 */
public class ReportEmployees {
    IReportEmployees employeesContext;

    public ReportEmployees(IReportEmployees employeesContext) {
        this.employeesContext = employeesContext;
    }

    public IReportEmployees getEmployeesContext() {
        return employeesContext;
    }
}
