package core.model.reports.impl;

import core.model.TimeTableEmployee;

import java.util.List;

/**
 * Created by ADMIN on 22.04.2018.
 */
public interface IReportTimeTable {
    void createReport(List<TimeTableEmployee> list);
}
