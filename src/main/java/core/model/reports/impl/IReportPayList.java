package core.model.reports.impl;

import core.model.TimeTableEmployee;

import java.util.List;

/**
 * Created by BigZy on 04.05.2018.
 */
public interface IReportPayList {
    void createReport(TimeTableEmployee timeTableEmployee);
    void createAllReports(List<TimeTableEmployee> list);
}
