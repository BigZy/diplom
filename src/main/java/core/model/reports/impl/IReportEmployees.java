package core.model.reports.impl;

import core.model.Employee;

import java.util.List;

/**
 * Created by BigZy on 04.02.2018.
 */
public interface IReportEmployees {
    void createReport(List<Employee> list);
}
