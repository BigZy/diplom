package core.model.reports;

import core.model.reports.impl.IReportTimeTable;

/**
 * Created by ADMIN on 22.04.2018.
 */
public class ReportTimeTable {
    IReportTimeTable timeTableContext;

    public ReportTimeTable(IReportTimeTable timeTableContext) {
        this.timeTableContext = timeTableContext;
    }

    public IReportTimeTable getTimeTableContext() {
        return timeTableContext;
    }
}
