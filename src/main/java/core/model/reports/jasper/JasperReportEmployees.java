package core.model.reports.jasper;

import core.model.Employee;
import core.model.reports.impl.IReportEmployees;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.awt.*;
import java.util.List;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by BigZy on 04.02.2018.
 */
public class JasperReportEmployees implements IReportEmployees {
    private static final Logger _log = Logger.getLogger(JasperReportEmployees.class);

    static {
        _log.setLevel(Level.OFF);
    }

    @Override
    public void createReport(List<Employee> list) {
        DRDataSource dataSource = new DRDataSource("№", "fullName", "dateBirth", "gender", "profession", "rate", "dateStartOfWork", "address");

        for (Employee e : list) {
            dataSource.add(e.getId(), e.getFullName(), e.getDateBirth(), e.getGender(), e.getProfession(), e.getRate(), e.getDateStartOfWork(), e.getAddress());
        }

        StyleBuilder boldStyle = stl.style().bold();
        StyleBuilder boldCenteredStyle = stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        StyleBuilder columnTitleStyle = stl.style(boldCenteredStyle)
                .setBorder(stl.pen1Point())
                .setBackgroundColor(Color.LIGHT_GRAY);
        try {
            JasperReportBuilder jasperReportBuilder = report();

            jasperReportBuilder// create new report design
                    .setColumnTitleStyle(columnTitleStyle)
                    .highlightDetailEvenRows()
                    .columns(// add columns
                            // title, field name data type
                            col.column("№", "№", type.integerType()),
                            col.column("ФИО", "fullName", type.stringType()),
                            col.column("Дата рождения", "dateBirth", type.dateType()),
                            col.column("Пол", "gender", type.stringType()),
                            col.column("Профессия", "profession", type.stringType()),
                            col.column("Тарифная ставка", "rate", type.integerType()),
                            col.column("Дата устройства", "dateStartOfWork", type.dateType()),
                            col.column("Адрес Проживания", "address", type.stringType()))
                    .title(cmp.text("Таблица сотрудников").setStyle(boldCenteredStyle))// shows report title
                    .pageFooter(cmp.pageXofY().setStyle(boldCenteredStyle))// shows number of page at page footer
                    .setDataSource(dataSource)// set datasource
                    .setPageFormat(PageType.A4, PageOrientation.LANDSCAPE)
                    .show(false);// create and show report
        } catch (DRException e) {
            _log.info(e);
        }
    }
}
