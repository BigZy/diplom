package core.model.reports.jasper;

import core.model.TimeTableEmployee;
import core.model.reports.impl.IReportPayList;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by BigZy on 04.05.2018.
 */
public class JasperReportPayList implements IReportPayList {
    private static final Logger _log = Logger.getLogger(JasperReportPayList.class);

    static {
        _log.setLevel(Level.OFF);
    }

    @Override
    public void createReport(TimeTableEmployee timeTableEmployee) {
        JasperReport jasperReport;
        try {
            JasperDesign jd  = JRXmlLoader.load("resources/data/reportsTemplate/PayList.jrxml");
            jasperReport = JasperCompileManager.compileReport(jd);
            Map parameters = new HashMap();

            int countD = 0;
            int countH = 0;
            int oD = 0;
            int oH = 0;
            int bD = 0;
            int bH = 0;

            for (int i = 0; i < timeTableEmployee.getDays().length; i++) {
                switch (timeTableEmployee.getDays()[i].get().toLowerCase()) {
                    case "о": {
                        oD++;
                        break;
                    } case "б": {
                        bD++;
                        break;
                    } case "а": {
                        break;
                    } default: {
                        countD++;
                        countH += Integer.parseInt(timeTableEmployee.getDays()[i].get());
                        break;
                    }
                }
                if (timeTableEmployee.getDays()[i].get().toLowerCase().equals("o")) {
                    oD++;
                    oH += Integer.parseInt(timeTableEmployee.getDays()[i].get());
                }
            }

            int pay = countH * timeTableEmployee.getRate();
            int oPay = oD * 8 * timeTableEmployee.getRate() - (oD * 8 * timeTableEmployee.getRate() / 100 * 25);
            int bPay = bD * 8 * timeTableEmployee.getRate() - (bD * 8 * timeTableEmployee.getRate() / 100 * 40);

            int sumpay = pay + oPay + bPay;
            int unPay = sumpay / 100 * 13;

            parameters.put("fullName", timeTableEmployee.getFullName());
            parameters.put("countD", countD);
            parameters.put("countH", countH);
            parameters.put("pay", pay);
            parameters.put("oD", oD);
            parameters.put("oH", oH);
            parameters.put("oPay", oPay);
            parameters.put("bD", bD);
            parameters.put("bH", bH);
            parameters.put("bPay", bPay);
            parameters.put("nD", 10);
            parameters.put("nH", 80);
            parameters.put("nPay", 600);
            parameters.put("unpay", unPay);
            parameters.put("sumpay", sumpay);
            parameters.put("sumUnpay", unPay);
            List<Object> list = new ArrayList<>();
            list.add("name");
            JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(list);
            JasperPrint jrprint = JasperFillManager.fillReport(jasperReport, parameters, ds);
            JasperViewer jv = new JasperViewer(jrprint, false);
            jv.setVisible(true);
        } catch (JRException e) {
            _log.info(e);
        }
    }

    @Override
    public void createAllReports(List<TimeTableEmployee> list) {

    }
}
