package core.model.reports.jasper;

import core.model.TimeTableEmployee;
import core.model.reports.impl.IReportTimeTable;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.awt.*;
import java.util.List;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

/**
 * Created by ADMIN on 22.04.2018.
 */
public class JasperReportTimeTable implements IReportTimeTable {
    private static final Logger _log = Logger.getLogger(JasperReportTimeTable.class);

    static {
        _log.setLevel(Level.OFF);
    }

    @Override
    public void createReport(List<TimeTableEmployee> list) {
        TimeTableEmployee test = list.get(0);
        String[] columns = new String[test.getDays().length + 3];
        columns[0] = "№";
        columns[1] = "fullName";
        for (int i = 0; i < test.getDays().length; i++) {
            columns[i + 2] = String.valueOf(i + 1);
        }
        columns[columns.length - 1] = "nightlyHour";

        DRDataSource dataSource = new DRDataSource(columns);

        for (TimeTableEmployee e : list) {
            Object[] o = new Object[columns.length];
            o[0] = e.getId();
            o[1] = e.getFullName();
            for (int i = 0; i < e.getDays().length; i++) {
                o[i + 2] = e.getDays()[i].get();
            }
            o[o.length - 1] = e.getNightlyHour();
            dataSource.add(o);
        }

        StyleBuilder boldStyle = stl.style().bold();
        StyleBuilder boldCenteredStyle = stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        StyleBuilder columnTitleStyle = stl.style(boldCenteredStyle)
                .setBorder(stl.pen1Point())
                .setBackgroundColor(Color.LIGHT_GRAY);

        JasperReportBuilder jasperReportBuilder = report();

        jasperReportBuilder.columns(col.column("№", "№", type.integerType()).setWidth(20));
        jasperReportBuilder.columns(col.column("ФИО", "fullName", type.stringType()).setWidth(150));
        for (int i = 0; i < columns.length - 3; i++) {
            jasperReportBuilder.columns(col.column(columns[i + 2], columns[i + 2], type.stringType()).setWidth(10));
        }
        jasperReportBuilder.columns(col.column("Ночные", "nightlyHour", type.integerType()).setWidth(40));

        StyleBuilder style = stl.style();
        style.setLeftBorder(stl.pen1Point());
        style.setRightBorder(stl.pen1Point());
        jasperReportBuilder.setColumnStyle(style);
        try {
            jasperReportBuilder// create new report design
                    .setColumnTitleStyle(columnTitleStyle)
                    .highlightDetailEvenRows()
                    .title(cmp.text("Таблица учета рабочего времени").setStyle(boldCenteredStyle))// shows report title
                    .pageFooter(cmp.pageXofY().setStyle(boldCenteredStyle))// shows number of page at page footer
                    .setDataSource(dataSource)// set datasource
                    .setPageFormat(PageType.A4, PageOrientation.LANDSCAPE)
                    .show(false);// create and show report
        } catch (DRException e) {
            _log.info(e);
        }
    }
}
