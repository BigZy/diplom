package core.model.reports;

import core.model.reports.impl.IReportPayList;

/**
 * Created by BigZy on 04.05.2018.
 */
public class ReportPayList {
    IReportPayList payListContext;

    public ReportPayList(IReportPayList payListContext) {
        this.payListContext = payListContext;
    }

    public IReportPayList getPayListContext() {
        return payListContext;
    }
}
