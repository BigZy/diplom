package core.model.components;

import core.model.Employee;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.Date;

/**
 * Created by BigZy on 04.02.2018.
 */
public class TableViewEmployee<S> extends TableView<S> {
    private TableColumn<Employee, String> colFullName, colGender, colProfession, colAddress;
    private TableColumn<Employee, Integer> id, colRate;
    private TableColumn<Employee, Date> colDateBirth, colDateStartOfWork;

    public TableViewEmployee() {
        //настройка таблицы и инициализация данных к ней
        id.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        colFullName.setCellValueFactory(cellData -> cellData.getValue().fullNameProperty());
        colDateBirth.setCellValueFactory(cellData -> cellData.getValue().dateBirthProperty());
        colGender.setCellValueFactory(cellData -> cellData.getValue().genderProperty());
        colProfession.setCellValueFactory(cellData -> cellData.getValue().professionProperty());
        colRate.setCellValueFactory(cellData -> cellData.getValue().rateProperty().asObject());
        colDateStartOfWork.setCellValueFactory(cellData -> cellData.getValue().dateStartOfWorkProperty());
        colAddress.setCellValueFactory(cellData -> cellData.getValue().addressProperty());
        //динамичный размер колонок в прцоентах
        id.prefWidthProperty().bind(this.widthProperty().multiply(0.02));
        colFullName.prefWidthProperty().bind(this.widthProperty().multiply(0.20));
        colDateBirth.prefWidthProperty().bind(this.widthProperty().multiply(0.09));
        colGender.prefWidthProperty().bind(this.widthProperty().multiply(0.15));
        colProfession.prefWidthProperty().bind(this.widthProperty().multiply(0.15));
        colRate.prefWidthProperty().bind(this.widthProperty().multiply(0.10));
        colDateStartOfWork.prefWidthProperty().bind(this.widthProperty().multiply(0.09));
        colAddress.prefWidthProperty().bind(this.widthProperty().multiply(0.20));
    }
}
