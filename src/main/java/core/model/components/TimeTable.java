package core.model.components;


import core.controllers.EnterTimeTableValue;
import core.model.Employee;
import core.model.TimeTableEmployee;
import core.model.reports.ReportPayList;
import core.model.reports.jasper.JasperReportPayList;
import core.resource.Scenes;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static core.App.getParserTimeTableEmployees;

/**
 * Created by ADMIN on 21.04.2018.
 */
public class TimeTable extends TableView {
    private TableColumn<TimeTableEmployee, Integer> idCol = new TableColumn<>("№");
    private TableColumn<TimeTableEmployee, String> fullNameCol = new TableColumn<>("ФИО");
    private List<TableColumn<TimeTableEmployee, String>> columnsDays = new ArrayList<>();
    private TableColumn<TimeTableEmployee, Integer> nightlyHourCol = new TableColumn<>("Ночные");

    private static Logger _log = Logger.getLogger(TimeTable.class);

    public TimeTable() {
        super();

        idCol.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        fullNameCol.setCellValueFactory(cellData -> cellData.getValue().fullNameProperty());

        this.getColumns().add(idCol);
        this.getColumns().add(fullNameCol);

        LocalDate localDate = LocalDate.now();

        double width = 0;
        Integer[] days = new Integer[localDate.lengthOfMonth()];
        for (int i = 1; i <= localDate.lengthOfMonth(); i++) {
            TableColumn<TimeTableEmployee, String> day = new TableColumn<>(String.valueOf(i));

            LocalDate kostyl = LocalDate.of(localDate.getYear(), localDate.getMonth(), i);
            if (kostyl.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals("Sunday") ||
                    kostyl.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH).equals("Saturday")) {
                day.setStyle("-fx-background-color: lightgray");
            }
            final int var = i - 1;
            days[var] = 0;
            day.setCellValueFactory(cellData -> cellData.getValue().getDays()[var]);
            day.prefWidthProperty().bind(this.widthProperty().multiply(0.023));
            day.setCellFactory(TextFieldEditingCell.forTableColumn());
            columnsDays.add(day);
            width += 0.023;
        }

        this.getColumns().addAll(columnsDays);

        idCol.prefWidthProperty().bind(this.widthProperty().multiply(0.03));
        fullNameCol.prefWidthProperty().bind(this.widthProperty().multiply(0.911 - width));

        /*nightlyHourCol.setCellValueFactory(cellData -> cellData.getValue().nightlyHourProperty().asObject());
        nightlyHourCol.prefWidthProperty().bind(this.widthProperty().multiply(0.045));
        nightlyHourCol.setCellFactory(c -> new IntegerEditingCell());*/
        //this.getColumns().add(nightlyHourCol);

        // Create ContextMenu
        ContextMenu contextMenu = new ContextMenu();
        MenuItem item1 = new MenuItem("Изменить выбранные");
        MenuItem item2 = new MenuItem("Рассчитать");
        item1.setOnAction(e -> {
            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setResizable(false);
            dialogStage.setScene(Scenes.getScenesHashMap().get("EnterTimeTableValue"));
            dialogStage.setTitle("Изменить ячейки");
            EnterTimeTableValue controller = (EnterTimeTableValue) Scenes.getControllerHashMap().get("controllerEnterTimeTableValue");
            controller.setDialogStage(dialogStage);
            controller.setTimeTable(this);
            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();
        });
        item2.setOnAction(e -> {
            Runnable runnable = () -> {
                try {
                    TimeUnit.SECONDS.sleep(0);
                    ReportPayList reportPayList;
                    reportPayList = new ReportPayList(new JasperReportPayList());
                    TimeTableEmployee timeTableEmployee = (TimeTableEmployee) this.getSelectionModel().getSelectedItem();
                    reportPayList.getPayListContext().createReport(timeTableEmployee);
                } catch (InterruptedException x) {
                    _log.info(x);
                }
            };

            Thread thread = new Thread(runnable);
            thread.start();
        });
        contextMenu.getItems().addAll(item1, item2);
        //слушаем мышку и клавиатуру
        this.setOnMouseClicked(e -> {
            if (e.getButton().toString().equals("SECONDARY")) {
                contextMenu.show(this, e.getScreenX(), e.getScreenY());
            } else {
                if (contextMenu.isShowing())
                    contextMenu.hide();
            }
        });

        this.setEditable(true);
        this.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.getSelectionModel().setCellSelectionEnabled(true);
        this.getItems().addAll(getParserTimeTableEmployees().getTimeTableEmployeesContext().getTimeTableEmployees());
    }

    public void saveTimeTable() {
        getParserTimeTableEmployees().getTimeTableEmployeesContext().saveTimeTableEmployees(this.getItems());
    }

    public void reloadTimeTableFromTable() {
        this.getItems().clear();
        this.setItems(getParserTimeTableEmployees().getTimeTableEmployeesContext().getTimeTableEmployees());
        this.refresh();
    }

    public void addTimeTableEmployeeFromTable(Employee empl) {
        String[] days = new String[LocalDate.now().lengthOfMonth()];

        for (int i = 0; i < days.length; i++) {
            days[i] = "0";
        }
        TimeTableEmployee timeTableEmployee = new TimeTableEmployee(empl, days, 0);
        this.getItems().add(timeTableEmployee);
    }

    public void removeEmployeeFromTable(int id) {
        int index = -1;

        for (int i = 0; i < getParserTimeTableEmployees().getTimeTableEmployeesContext().getTimeTableEmployees().size(); i++) {
            if (id == getParserTimeTableEmployees().getTimeTableEmployeesContext().getTimeTableEmployees().get(i).getId()) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            System.out.println("delete");
            this.getItems().remove(index);
        }

        reloadTimeTableFromTable();
    }
}
