package core.model.components;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Created by BigZy on 11.01.2018.
 */
//TODO при смене фокуса без нажатия enter не сохраняет значения, пофиксить
public class IntegerEditingCell<S> extends TableCell<S, Integer> {
    private TextField textField = new TextField();
    int click = 0;

    public IntegerEditingCell() {
        textField.setTextFormatter(new TextFormatter<Integer>(change ->
                change.getControlNewText().matches("\\d*") ? change : null));
        textField.setOnAction(e -> {
            if (textField.getText().isEmpty()) {
                commitEdit(0);
            } else {
                commitEdit(Integer.parseInt(textField.getText()));
            }
        });
        textField.addEventFilter(KeyEvent.KEY_RELEASED, e -> {
            if (e.getCode()== KeyCode.ESCAPE) {
                cancelEdit();
            }
        });
        textField.setOnMouseClicked(e -> {
            click++;

            if (click > 1) {
                commitEdit(Integer.parseInt(textField.getText()));
                click = 0;
            }
        });
        setOnDragDetected(e -> {
            startFullDrag();
            getTableColumn().getTableView().getSelectionModel().select(getIndex(), getTableColumn());
        });
        setOnMouseDragEntered(e -> {
            getTableColumn().getTableView().getSelectionModel().select(getIndex(), getTableColumn());
        });
        setGraphic(textField);
    }

    @Override
    protected void updateItem(Integer item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText("");
            setContentDisplay(ContentDisplay.TEXT_ONLY);
            return ;
        }
        if (isEditing()) {
            textField.setText(item.toString());
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        } else {
            setText(item.toString());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }
    }
    @Override
    public void startEdit() {
        super.startEdit();
        textField.setText(getItem().toString());
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getItem().toString());
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }
}
