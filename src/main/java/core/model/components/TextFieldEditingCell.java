package core.model.components;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.Event;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import javafx.util.StringConverter;
import javafx.util.converter.DefaultStringConverter;

public class TextFieldEditingCell<S,T> extends TableCell<S,T> {
    public static <S> Callback<TableColumn<S,String>, TableCell<S,String>> forTableColumn() {
        return forTableColumn(new DefaultStringConverter());
    }

    public static <S,T> Callback<TableColumn<S,T>, TableCell<S,T>> forTableColumn(
            final StringConverter<T> converter) {
        return list -> new TextFieldEditingCell<S,T>(converter);
    }

    private TextField textField;
    private boolean escapePressed=false;
    private TablePosition<S, ?> tablePos=null;

    public TextFieldEditingCell() {
        this(null);
        setOnDragDetected(e -> {
            startFullDrag();
            getTableColumn().getTableView().getSelectionModel().select(getIndex(), getTableColumn());
        });
        setOnMouseDragEntered(e -> {
            getTableColumn().getTableView().getSelectionModel().select(getIndex(), getTableColumn());
        });
    }

    public TextFieldEditingCell(StringConverter<T> converter) {
        this.getStyleClass().add("text-field-table-cell");
        setOnDragDetected(e -> {
            startFullDrag();
            getTableColumn().getTableView().getSelectionModel().select(getIndex(), getTableColumn());
        });
        setOnMouseDragEntered(e -> {
            getTableColumn().getTableView().getSelectionModel().select(getIndex(), getTableColumn());
        });
        setConverter(converter);
    }

    private ObjectProperty<StringConverter<T>> converter =
            new SimpleObjectProperty<StringConverter<T>>(this, "converter");

    public final ObjectProperty<StringConverter<T>> converterProperty() {
        return converter;
    }

    public final void setConverter(StringConverter<T> value) {
        converterProperty().set(value);
    }

    public final StringConverter<T> getConverter() {
        return converterProperty().get();
    }

    @Override public void startEdit() {
        if (! isEditable()
                || ! getTableView().isEditable()
                || ! getTableColumn().isEditable()) {
            return;
        }
        super.startEdit();
        System.out.println("start edit");

        if (isEditing()) {
            if (textField == null) {
                textField = getTextField();
            }
            escapePressed=false;
            startEdit(textField);
            final TableView<S> table = getTableView();
            tablePos=table.getEditingCell();
        }
    }

    /** {@inheritDoc} */
    @Override public void commitEdit(T newValue) {
        if (! isEditing())
            return;

        System.out.println("commit edit");

        final TableView<S> table = getTableView();
        if (table != null) {
            // Inform the TableView of the edit being ready to be committed.
            CellEditEvent editEvent = new CellEditEvent(
                    table,
                    tablePos,
                    TableColumn.editCommitEvent(),
                    newValue
            );

            Event.fireEvent(getTableColumn(), editEvent);
        }

        // we need to setEditing(false):
        super.cancelEdit(); // this fires an invalid EditCancelEvent.

        // update the item within this cell, so that it represents the new value
        updateItem(newValue, false);

        if (table != null) {
            // reset the editing cell on the TableView
            table.edit(-1, null);

            // request focus back onto the table, only if the current focus
            // owner has the table as a parent (otherwise the user might have
            // clicked out of the table entirely and given focus to something else.
            // It would be rude of us to request it back again.
            // requestFocusOnControlOnlyIfCurrentFocusOwnerIsChild(table);
        }
    }


    /** {@inheritDoc} */
    @Override public void cancelEdit() {
        System.out.println("cancel");
        if(escapePressed) {
            // this is a cancel event after escape key
            super.cancelEdit();
            setText(getItemText()); // restore the original text in the view
        }
        else {
            // this is not a cancel event after escape key
            // we interpret it as commit.
            String newText=textField.getText(); // get the new text from the view
            this.commitEdit(getConverter().fromString(newText)); // commit the new text to the model
        }
        setGraphic(null); // stop editing with TextField

    }

    /** {@inheritDoc} */
    @Override public void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);
        updateItem();
    }

    /***************************************************************************
     *                                                                         *
     *  // djw code taken and adapted from package protected CellUtils.        *
     *                                                                         *
     **************************************************************************/

    private TextField getTextField() {

        final TextField textField = new TextField(getItemText());

        // Use onAction here rather than onKeyReleased (with check for Enter),
        // as otherwise we encounter RT-34685
        textField.setOnAction(event -> {
            if (converter == null) {
                throw new IllegalStateException(
                        "Attempting to convert text input into Object, but provided "
                                + "StringConverter is null. Be sure to set a StringConverter "
                                + "in your cell factory.");
            }
            this.commitEdit(getConverter().fromString(textField.getText()));
            event.consume();
        });
        textField.setOnKeyPressed(t -> { if (t.getCode() == KeyCode.ESCAPE) escapePressed = true; else escapePressed = false; });
        textField.setOnKeyReleased(t -> {
            if (t.getCode() == KeyCode.ESCAPE) {
                // djw the code may depend on java version / expose incompatibilities:
                throw new IllegalArgumentException("did not expect esc key releases here.");
            }
        });
        return textField;
    }

    private String getItemText() {
        return getConverter() == null ?
                getItem() == null ? "" : getItem().toString() :
                getConverter().toString(getItem());
    }

    private void updateItem() {
        if (isEmpty()) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getItemText());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getItemText());
                setGraphic(null);
            }
        }
    }

    private void startEdit(final TextField textField) {
        if (textField != null) {
            textField.setText(getItemText());
        }
        setText(null);
        setGraphic(textField);
        textField.selectAll();

        // requesting focus so that key input can immediately go into the
        // TextField (see RT-28132)
        textField.requestFocus();
    }
}