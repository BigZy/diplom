package core.model.components;

import core.controllers.Menu;
import core.model.Employee;
import core.model.EmployeeFactory;
import core.resource.Scenes;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static core.App.getParserEmployees;

/**
 * Created by ADMIN on 21.04.2018.
 */
public class EmployeesTable extends TableView {
    private TableColumn<Employee, Integer> idCol = new TableColumn<>("№");
    private TableColumn<Employee, String> fullNameCol = new TableColumn<>("ФИО");
    private TableColumn<Employee, Date> dateBirthCol = new TableColumn<>("Дата рождения");
    private TableColumn<Employee, String> genderCol = new TableColumn<>("Пол");
    private TableColumn<Employee, String> professionCol = new TableColumn<>("Профессия");
    private TableColumn<Employee, String> categoryRate = new TableColumn<>("Вид ставки");
    private TableColumn<Employee, Integer> rateCol = new TableColumn<>("Ставка");
    private TableColumn<Employee, Date> dateStartOfWork = new TableColumn<>("Дата устройства");
    private TableColumn<Employee, String> addressCol = new TableColumn<>("Адрес проживания");

    public int lastId = 1;

    public EmployeesTable() {
        super();
        //настройка таблицы и инициализация данных к ней
        idCol.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        fullNameCol.setCellValueFactory(cellData -> cellData.getValue().fullNameProperty());
        dateBirthCol.setCellValueFactory(cellData -> cellData.getValue().dateBirthProperty());
        genderCol.setCellValueFactory(cellData -> cellData.getValue().genderProperty());
        professionCol.setCellValueFactory(cellData -> cellData.getValue().professionProperty());
        categoryRate.setCellValueFactory(cellData -> cellData.getValue().categoryRateProperty());
        rateCol.setCellValueFactory(cellData -> cellData.getValue().rateProperty().asObject());
        dateStartOfWork.setCellValueFactory(cellData -> cellData.getValue().dateStartOfWorkProperty());
        addressCol.setCellValueFactory(cellData -> cellData.getValue().addressProperty());
        //динамичный размер колонок в прцоентах
        idCol.prefWidthProperty().bind(this.widthProperty().multiply(0.02));
        fullNameCol.prefWidthProperty().bind(this.widthProperty().multiply(0.15));
        dateBirthCol.prefWidthProperty().bind(this.widthProperty().multiply(0.09));
        genderCol.prefWidthProperty().bind(this.widthProperty().multiply(0.15));
        professionCol.prefWidthProperty().bind(this.widthProperty().multiply(0.13));
        categoryRate.prefWidthProperty().bind(this.widthProperty().multiply(0.07));
        rateCol.prefWidthProperty().bind(this.widthProperty().multiply(0.10));
        dateStartOfWork.prefWidthProperty().bind(this.widthProperty().multiply(0.09));
        addressCol.prefWidthProperty().bind(this.widthProperty().multiply(0.20));
        //теперь ячейки можно редактировать прямо в таблице с дальнейшим сохранением
        fullNameCol.setCellFactory(TextFieldEditingCell.forTableColumn());
        Callback<TableColumn<Employee, Date>, TableCell<Employee, Date>> crcDateCellFactory = (
                TableColumn<Employee, Date> param) -> new DateEditingCell();
        dateBirthCol.setCellFactory(crcDateCellFactory);
        genderCol.setCellFactory(ComboBoxTableCell.forTableColumn("Мужской", "Женский"));
        professionCol.setCellFactory(ComboBoxTableCell.forTableColumn("Продавец-консультант", "Продавец-кассир", "Грузчик", "Администратор", "Управляющий", "Бухгалтер", "Директор", "Кладовщик", "Уборщик"));
        categoryRate.setCellFactory(ComboBoxTableCell.forTableColumn("Оклад", "Почасовая", "Сдельная"));
        rateCol.setCellFactory(c -> new IntegerEditingCell());
        dateStartOfWork.setCellFactory(crcDateCellFactory);
        addressCol.setCellFactory(TextFieldEditingCell.forTableColumn());
        //add columns
        this.getColumns().add(idCol);
        this.getColumns().add(fullNameCol);
        this.getColumns().add(dateBirthCol);
        this.getColumns().add(genderCol);
        this.getColumns().add(professionCol);
        this.getColumns().add(categoryRate);
        this.getColumns().add(rateCol);
        this.getColumns().add(dateStartOfWork);
        this.getColumns().add(addressCol);

        // Create ContextMenu
        ContextMenu contextMenu = new ContextMenu();
        MenuItem item1 = new MenuItem("Добавить");
        MenuItem item2 = new MenuItem("Удалить");
        item1.setOnAction(e -> {
            addEmployeeFromTable();
        });
        item2.setOnAction(e -> {
            removeEmployeeFromTable();
        });

        contextMenu.getItems().addAll(item1, item2);
        //слушаем мышку и клавиатуру
        this.setOnMouseClicked(e -> {
            if (e.getButton().toString().equals("SECONDARY")) {
                contextMenu.show(this, e.getScreenX(), e.getScreenY());
            } else {
                if (contextMenu.isShowing())
                    contextMenu.hide();
            }
        });
        this.setOnKeyPressed(e -> {
            if (e.getCode().getName().equals("Delete")) {
                removeEmployeeFromTable();
            }
        });

        final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.S,
                KeyCombination.CONTROL_DOWN);
        this.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
            if (keyComb1.match(event)) {
                saveEmployees();
            }
        });

        //инициализация данных в таблицу
        this.setItems(getParserEmployees().getEmployeesContext().getEmployees());

        this.setEditable(true);
        this.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        this.getSelectionModel().setCellSelectionEnabled(true);
    }

    public boolean saveEmployees() {
        boolean valid = true;
        Menu menu = (Menu) Scenes.getControllerHashMap().get("controllerMenu");

        if (!this.getItems().isEmpty()) {
            for (Object o : this.getItems()) {
                Employee employee = (Employee) o;
                if (employee.getFullName().isEmpty() || employee.getProfession().isEmpty() || employee.getRate() == 0 || employee.getAddress().isEmpty()) {
                    menu.setErrorEmployeesTable("Заполните все поля!");
                    this.getSelectionModel().select(employee);
                    valid = false;

                    break;
                }
            }

            if (valid) {
                menu.setErrorEmployeesTable("");
                getParserEmployees().getEmployeesContext().saveEmployees(this.getItems());
            }
        } else {
            getParserEmployees().getEmployeesContext().saveEmployees(this.getItems());
        }

        return valid;
    }

    public void addEmployeeFromTable() {
        EmployeeFactory<Employee> personFactory = Employee::new;
        Employee person = personFactory.createEmployee(getLastId(), "", new Date(), "Мужской", "Продавец-консультант", "Оклад" , 0, new Date(), "");
        this.getItems().add(person);
    }

    public void removeEmployeeFromTable() {
        if (this.getSelectionModel().getSelectedItem() != null)
            this.getItems().remove(this.getSelectionModel().getSelectedItem());
    }

    public void reloadEmployeeFromTable() {
        this.getItems().clear();
        this.setItems(getParserEmployees().getEmployeesContext().getEmployees());
        this.refresh();
    }

    public int getLastId() {
        if (!this.getItems().isEmpty()) {
            Set<Integer> before = new HashSet<>();
            for (Object o : this.getItems()) {
                Employee employee = (Employee) o;
                before.add(employee.getId());
            }

            Object[] affter = before.toArray();
            Arrays.sort(affter);

            lastId = (int) affter[affter.length - 1] + 1;

            return lastId;
        } else
            lastId = 1;
            return lastId;
    }
}
