package core.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by ADMIN on 19.04.2018.
 */
public class TimeTableEmployee extends Employee {
    private StringProperty[] days;
    private IntegerProperty nightlyHour;

    public TimeTableEmployee(Employee e, String[] days, Integer nightlyHour) {
        super(e.getId(), e.getFullName(), e.getDateBirth(), e.getGender(), e.getProfession(), e.getCategoryRate() , e.getRate(), e.getDateStartOfWork(), e.getAddress());
        this.days = new StringProperty[days.length];
        this.nightlyHour = new SimpleIntegerProperty(nightlyHour);

        for (int i = 0; i < days.length; i++) {
            this.days[i] = new SimpleStringProperty(days[i]);
        }
    }

    public StringProperty[] getDays() {
        return days;
    }

    public void setDays(StringProperty[] days) {
        this.days = days;
    }

    public int getNightlyHour() {
        return nightlyHour.get();
    }

    public IntegerProperty nightlyHourProperty() {
        return nightlyHour;
    }

    public void setNightlyHour(int nightlyHour) {
        this.nightlyHour.set(nightlyHour);
    }
}
