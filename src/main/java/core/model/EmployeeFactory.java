package core.model;

import java.util.Date;

/**
 * Created by BigZy on 21.01.2018.
 */
public interface EmployeeFactory<E extends Employee> {
    E createEmployee(Integer id, String fullName, Date dateBirth, String gender, String profession, String categoryRate, Integer rate, Date dateStartOfWork, String address);
}
