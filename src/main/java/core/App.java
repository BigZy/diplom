package core;

import core.model.dataParser.ParserCompany;
import core.model.dataParser.ParserEmployees;
import core.model.dataParser.ParserTimeTable;
import core.model.dataParser.json.JsonCompanyParser;
import core.model.dataParser.json.JsonEmployeesParser;
import core.model.dataParser.json.JsonTimeTableParser;
import core.resource.Scenes;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import static core.resource.Config.loadConfiguration;
import static core.resource.Scenes.loadScenes;

/**
 * Created by BigZy on 09.01.2018.
 */
public class App extends Application {
    private static Stage primaryStage;
    private static ParserEmployees parserEmployees;
    private static ParserTimeTable parserTimeTableEmployees;
    private static ParserCompany parserCompany;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Орион");
        this.primaryStage.getIcons().add(new Image(App.class.getResourceAsStream("/images/logo.png")));
        this.primaryStage.setScene(Scenes.getScenesHashMap().get("Login"));
        this.primaryStage.setResizable(false);
        this.primaryStage.show();
    }

    public static void main(String[] args) {
        loadConfiguration();
        //зависимость
        parserEmployees = new ParserEmployees(new JsonEmployeesParser());
        parserEmployees.getEmployeesContext().loadEmployees();
        parserTimeTableEmployees = new ParserTimeTable(new JsonTimeTableParser());
        parserTimeTableEmployees.getTimeTableEmployeesContext().loadTimeTableEmployees();
        parserCompany = new ParserCompany(new JsonCompanyParser());
        parserCompany.getCompanyContext().loadCompany();
        loadScenes();
        launch(args);
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static ParserEmployees getParserEmployees() {
        return parserEmployees;
    }

    public static ParserTimeTable getParserTimeTableEmployees() {
        return parserTimeTableEmployees;
    }

    public static ParserCompany getParserCompany() {
        return parserCompany;
    }
}
