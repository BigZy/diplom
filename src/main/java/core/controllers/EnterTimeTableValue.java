package core.controllers;

import core.model.TimeTableEmployee;
import core.model.components.TimeTable;
import javafx.fxml.FXML;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Created by BigZy on 28.04.2018.
 */
public class EnterTimeTableValue {
    @FXML
    private TextField value;
    private Stage dialogStage;
    private TimeTable timeTable;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setTimeTable(TimeTable timeTable) {
        this.timeTable = timeTable;
    }

    public void handleSave() {
        timeTable.getSelectionModel().getSelectedCells().forEach(x -> {
            TablePosition test = (TablePosition) x;
            TimeTableEmployee employee = (TimeTableEmployee) timeTable.getSelectionModel().getSelectedItems().get(((TablePosition) x).getRow());
            employee.getDays()[test.getColumn() - 2].set(value.getText());
        });
        dialogStage.close();
    }

    public void handleCancel() {
        dialogStage.close();
    }
}
