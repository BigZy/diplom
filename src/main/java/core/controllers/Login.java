package core.controllers;

import core.App;
import core.crypt.MD5;
import core.resource.Config;
import core.resource.Scenes;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import org.apache.log4j.Logger;

/**
 * Created by BigZy on 09.01.2018.
 */
public class Login {
    @FXML
    private PasswordField password;
    @FXML
    private Label error;

    private static final Logger _log = Logger.getLogger(Login.class);

    public void login() {
        if (Config.PASSWORD.isEmpty() && password.getText().isEmpty()) {
            _log.error("Несанкционированный доступ!");
            System.exit(-1);
        } else if (MD5.encryptMD5(password.getText()).equals(Config.PASSWORD)) {
            App.getPrimaryStage().setScene(Scenes.getScenesHashMap().get("Menu"));
            App.getPrimaryStage().setResizable(true);
            App.getPrimaryStage().setMaximized(true);
        } else if (password.getText().isEmpty()) {
            error.setText("Введите пароль");
        } else {
            error.setText("Неверный пароль!");
        }
    }
}
