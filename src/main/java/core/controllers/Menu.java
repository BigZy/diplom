package core.controllers;

import core.App;
import core.model.Company;
import core.model.Employee;
import core.model.TimeTableEmployee;
import core.model.components.EmployeesTable;
import core.model.components.TimeTable;
import core.model.reports.ReportEmployees;
import core.model.reports.ReportTimeTable;
import core.model.reports.jasper.JasperReportEmployees;
import core.model.reports.jasper.JasperReportTimeTable;
import core.resource.Scenes;
import core.resource.Values;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static core.App.*;

/**
 * Created by BigZy on 09.01.2018.
 */
public class Menu {
    @FXML
    private ComboBox leader, accountant, typeTax;
    @FXML
    private TimeTable tableTime;
    @FXML
    private EmployeesTable tableEmployee;
    @FXML
    private Label year_month;
    @FXML
    private Button save, add, remove, reload, createReport, saveTimeTable, createReportTimeTable;
    @FXML
    private Label errorEmployeesTable;
    @FXML
    private TextField name, inn, ogrn, ogrnip, imns, pfr, fss, foms, kpp, oktmo, okved, okpo, okopf;

    private static final Logger _log = Logger.getLogger(Menu.class);

    @FXML
    void initialize() {
        Company company = getParserCompany().getCompanyContext().getCompany();
        name.setText(company.getName());
        inn.setText(company.getInn());
        ogrn.setText(company.getOgrn());
        ogrnip.setText(company.getOgrnip());
        imns.setText(company.getImns());
        pfr.setText(company.getPfr());
        fss.setText(company.getFss());
        foms.setText(company.getFoms());
        kpp.setText(company.getKpp());
        oktmo.setText(company.getOktmo());
        okved.setText(company.getOkved());
        okpo.setText(company.getOkpo());
        okopf.setText(company.getOkopf());


        tableEmployee.getItems().forEach(e -> {
            Employee empl = (Employee) e;
            switch (empl.professionProperty().get()) {
                case "Директор" : {
                    leader.getItems().add(empl.getFullName());
                    break;
                } case "Бухгалтер" : {
                    accountant.getItems().add(empl.getFullName());
                    break;
                }
            }
        });

        leader.getSelectionModel().selectFirst();
        accountant.getSelectionModel().selectFirst();
        typeTax.getItems().setAll("Стандартная форма", "Упрощенная система со льготами", "Упрощенная система без льгот");
        typeTax.getSelectionModel().selectFirst();

        setSettingsButton(save, Values.BUTTON_SAVE, App.class.getResource("/images/save.png").toString());
        setSettingsButton(add, "Добавить сотрудника", App.class.getResource("/images/add.png").toString());
        setSettingsButton(remove, "Удалить сотрудника", App.class.getResource("/images/delete.png").toString());
        setSettingsButton(reload, "Отменить все изменения", App.class.getResource("/images/reload.png").toString());
        setSettingsButton(createReport, "Создать отчет по сотрудникам", App.class.getResource("/images/report.png").toString());
        setSettingsButton(saveTimeTable, "Сохранить табель", App.class.getResource("/images/save.png").toString());
        setSettingsButton(createReportTimeTable, "Создать отчет по учету рабочего времени", App.class.getResource("/images/report.png").toString());
        LocalDate localDate = LocalDate.now();
        year_month.setText(localDate.getYear() + " " + localDate.getMonth().getDisplayName(TextStyle.FULL, new Locale("ru")));
    }

    public void handleWindowChangePassword(ActionEvent actionEvent) {
        // Создаём диалоговое окно Stage.
        Stage dialogStage = new Stage();
        dialogStage.getIcons().add(new Image(App.class.getResourceAsStream("/images/logo.png")));
        dialogStage.setResizable(false);
        dialogStage.setScene(Scenes.getScenesHashMap().get("ChangePassword"));
        dialogStage.setTitle("Смена пароля");
        ChangePassword controller = (ChangePassword) Scenes.getControllerHashMap().get("controllerChangePassword");
        controller.setDialogStage(dialogStage);
        // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
        dialogStage.showAndWait();
    }

    public void createReportByEmployees() {
        Runnable runnable = () -> {
            try {
                TimeUnit.SECONDS.sleep(0);
                ReportEmployees reportEmployees;
                reportEmployees = new ReportEmployees(new JasperReportEmployees());
                reportEmployees.getEmployeesContext().createReport(getParserEmployees().getEmployeesContext().getEmployees());
            } catch (InterruptedException e) {
                _log.error(e);
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void setSettingsButton(Button button, String text, String imgURL) {
        Tooltip tooltip = new Tooltip(text);
        ImageView image = new ImageView(new Image(imgURL));
        image.setFitHeight(25);
        image.setFitWidth(25);
        button.setGraphic(image);
        button.setTooltip(tooltip);
    }

    public void handleSaveEmployees() {
        if (tableEmployee.saveEmployees()) {
            List<Employee> employees = tableEmployee.getItems();
            List<TimeTableEmployee> timeTableEmployees = tableTime.getItems();
            List<TimeTableEmployee> after = new ArrayList<>();
            for (int i = 0; i < employees.size(); i++) {
                String[] days;
                if (i < timeTableEmployees.size()) {
                    TimeTableEmployee old = timeTableEmployees.get(i);
                    days = new String[old.getDays().length];
                    for (int i1 = 0; i1 < tableEmployee.getItems().size(); i1++) {
                        days[i1] = old.getDays()[i1].get();
                    }
                    TimeTableEmployee update = new TimeTableEmployee(employees.get(i), days, old.getNightlyHour());
                    after.add(update);
                } else {
                    days = new String[LocalDate.now().lengthOfMonth()];
                    for (int i1 = 0; i1 < days.length; i1++) {
                        days[i1] = "0";
                    }
                    TimeTableEmployee update = new TimeTableEmployee(employees.get(i), days, 0);
                    after.add(update);
                }
            }

            tableTime.getItems().clear();
            tableTime.getItems().addAll(after);
            tableTime.refresh();
            tableTime.saveTimeTable();
        }

        /*List<TimeTableEmployee> before = tableTime.getItems();
        List<TimeTableEmployee> after = new ArrayList<>();

        for (int i = 0; i < before.size(); i++) {
            TimeTableEmployee old = before.get(i);
            String[] days = new String[old.getDays().length];

            for (int i1 = 0; i1 < tableEmployee.getItems().size(); i1++) {
                days[i1] = old.getDays()[i1].get();
            }

            TimeTableEmployee update = new TimeTableEmployee((Employee) tableEmployee.getItems().get(i), days, old.getNightlyHour());
            after.add(update);
        }*/
    }

    public void handleAddEmployeeFromTable() {
        tableEmployee.addEmployeeFromTable();
    }

    public void handleRemoveEmployeeFromTable() {
        Employee employee = (Employee) tableEmployee.getSelectionModel().getSelectedItem();
        tableEmployee.removeEmployeeFromTable();
        //tableTime.removeEmployeeFromTable(employee.getId());
    }

    public void handleReloadEmployeeFromTable() {
        tableEmployee.reloadEmployeeFromTable();
    }

    public void setErrorEmployeesTable(String text) {
        errorEmployeesTable.setText(text);
    }

    public void handleSaveTimeTable() {
        tableTime.saveTimeTable();
    }

    public void handleReloadTimeTable() {
        tableTime.reloadTimeTableFromTable();
    }

    public void createReportByTableTime() {
        Runnable runnable = () -> {
            try {
                TimeUnit.SECONDS.sleep(0);
                ReportTimeTable reportTimeTable;
                reportTimeTable = new ReportTimeTable(new JasperReportTimeTable());
                reportTimeTable.getTimeTableContext().createReport(getParserTimeTableEmployees().getTimeTableEmployeesContext().getTimeTableEmployees());
            } catch (InterruptedException e) {
                _log.error(e);
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    public void handleShowRates(ActionEvent actionEvent) {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Тарифы страховых взносов");
        alert.setHeaderText(null);
        alert.setContentText("ПФР 876000 руб. 22% \n" +
                "ФСС 755000 руб. 2.9% \n" +
                "ФФОМС Облагается вся сумма 5.1%");

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(App.class.getResourceAsStream("/images/logo.png")));
        stage.showAndWait();
    }

    public void handleSaveCompany() {
        getParserCompany().getCompanyContext().saveCompany(new Company(name.getText(), inn.getText(), ogrn.getText(), ogrnip.getText(), imns.getText(), "123",
                pfr.getText(), fss.getText(), foms.getText(), typeTax.getSelectionModel().getSelectedItem().toString(), kpp.getText(), oktmo.getText(),
                okved.getText(), okpo.getText(), okopf.getText(), leader.getSelectionModel().getSelectedItem().toString(), accountant.getSelectionModel().getSelectedItem().toString()));
    }
}
