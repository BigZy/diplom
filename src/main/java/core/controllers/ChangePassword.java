package core.controllers;

import core.resource.Config;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

import static core.crypt.MD5.encryptMD5;

/**
 * Created by BigZy on 12.01.2018.
 */
public class ChangePassword {
    @FXML
    private Label error;
    @FXML
    private PasswordField oldPass, newPass, repeatPass;

    private Stage dialogStage;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void handleChangePassword(ActionEvent actionEvent) {
        String text;

        if (!oldPass.getText().isEmpty() && !newPass.getText().isEmpty() && !repeatPass.getText().isEmpty()) {
            if (encryptMD5(oldPass.getText()).equals(Config.PASSWORD)) {
                if (newPass.getText().equals(repeatPass.getText())) {
                    Config.setProperty("Password", encryptMD5(newPass.getText()));
                    Config.loadConfiguration();
                    text = "";
                    dialogStage.close();
                } else
                    text = "Пароль не изменён, так как новый пароль повторен неправильно.";
            } else
                text = "Пароль не изменён, так как прежний пароль введён неправильно.";
        } else
            text = "Заполните все поля.";

        error.setText(text);
    }
}
