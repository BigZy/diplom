package core.crypt;

import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by BigZy on 09.01.2018.
 */
public class MD5 {
    private static final Logger _log = Logger.getLogger(MD5.class);

    public static String encryptMD5(String to_encrypt){
        StringBuffer code = new StringBuffer(); //the hash code
        MessageDigest messageDigest = null;

        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            _log.error(e.getMessage());
        }

        byte bytes[] = to_encrypt.getBytes();
        byte digest[] = messageDigest.digest(bytes); //create code

        for (int i = 0; i < digest.length; ++i) {
            code.append(Integer.toHexString(0x0100 + (digest[i] & 0x00FF)).substring(1));
        }

        return new String(code.toString());
    }
}
