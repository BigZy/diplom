package core.resource;

/**
 * Created by ADMIN on 16.04.2018.
 */
public class Values {
    public static final String BUTTON_SAVE = "Сохранить все данные (Ctrl+S)";
    public static final String BUTTON_RELOAD = "Отменить все изменения";
    public static final String BUTTON_CREATE = "Добавить сотрудника";
    public static final String BUTTON_DELETE = "Удалить сотрудника";
}
