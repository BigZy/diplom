package core.resource;

import core.App;
import core.controllers.ChangePassword;
import core.controllers.EnterTimeTableValue;
import core.controllers.Login;
import core.controllers.Menu;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by BigZy on 09.01.2018.
 */
public class Scenes {
    private static HashMap<String, Scene> scenesHashMap = new HashMap<>();
    private static HashMap<String, Object> controllerHashMap = new HashMap<>();

    private static final Logger _log = Logger.getLogger(Scenes.class);

    public static void loadScenes() {
        try {

            FXMLLoader loginLoader = new FXMLLoader();
            loginLoader.setLocation(App.class.getResource("/views/Login.fxml"));
            AnchorPane loginPage = loginLoader.load();
            Login loginController = loginLoader.getController();
            Scene loginScene = new Scene(loginPage, 351, 165);
            //вешаем слушатель клавиатуры
            //TODO как то переделать :/
            loginScene.setOnKeyPressed(event -> {
                if (event.getCode().getName().equals("Enter")) {
                    loginController.login();
                }
            });

            FXMLLoader menuLoader = new FXMLLoader();
            menuLoader.setLocation(App.class.getResource("/views/Menu.fxml"));
            AnchorPane menuPage = menuLoader.load();
            Menu menuController = menuLoader.getController();
            Scene menuScene = new Scene(menuPage, 1200, 800);
            System.out.println("load scenes");

            FXMLLoader changePasswordloader = new FXMLLoader();
            changePasswordloader.setLocation(App.class.getResource("/views/ChangePassword.fxml"));
            AnchorPane changePasswordPage = changePasswordloader.load();
            ChangePassword changePasswordController = changePasswordloader.getController();
            Scene changePasswordScene = new Scene(changePasswordPage);

            FXMLLoader enterTimeTableValueLoader = new FXMLLoader();
            enterTimeTableValueLoader.setLocation(App.class.getResource("/views/EnterTimeTableValue.fxml"));
            AnchorPane enterTimeTableValuePage = enterTimeTableValueLoader.load();
            EnterTimeTableValue enterTimeTableValueController = enterTimeTableValueLoader.getController();
            Scene enterTimeTableValueScene = new Scene(enterTimeTableValuePage);

            scenesHashMap.put("Login", loginScene);
            controllerHashMap.put("controllerLogin", loginController);
            scenesHashMap.put("Menu", menuScene);
            controllerHashMap.put("controllerMenu", menuController);
            scenesHashMap.put("ChangePassword", changePasswordScene);
            controllerHashMap.put("controllerChangePassword", changePasswordController);
            scenesHashMap.put("EnterTimeTableValue", enterTimeTableValueScene);
            controllerHashMap.put("controllerEnterTimeTableValue", enterTimeTableValueController);

            //инициализация стиля
            menuPage.getStylesheets().add(App.class.getResource("/css/style.css").toExternalForm());
        } catch (IOException e) {
            e.printStackTrace();
            _log.error(e);
        }
    }

    public static HashMap<String, Scene> getScenesHashMap() {
        return scenesHashMap;
    }

    public static HashMap<String, Object> getControllerHashMap() {
        return controllerHashMap;
    }
}
