package core.resource;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by ADMIN on 06.04.2017.
 */
public class Config {
    private static final Logger _log = Logger.getLogger(Config.class);

    private static Properties properties;
    private static File file;
    public static String PASSWORD;

    public static void loadConfiguration()
    {
        file = new File("config.ini");
        properties = new Properties();

        try {
            properties.load(new FileInputStream(file));
        } catch (IOException e) {
            _log.error(e.getMessage());
        }

        PASSWORD = properties.getProperty("Password");
    }

    public static Properties getProperties() {
        return properties;
    }

    public static void setProperty(String key, String value) {
        try {
            //Устанавливаем значение свойста
            properties.setProperty(key, value);
            //Сохраняем свойства в файл.
            properties.store(new FileOutputStream(file), null);
            //перезагружаем
            loadConfiguration();
        } catch (IOException e) {
            _log.info("ошибка сохранения конфига " + e);
        }
    }
}
